terraform {
  backend "s3" {
    bucket = "fuku-terraform"
    key    = "vm-foundation/terraform"
    region = "us-east-1"
  }
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.14"
    }
  }
}

provider "proxmox" {
  pm_debug = true
}

module "master1" {
  source     = "../modules/proxmox-vm"
  vm_id      = 3001
  vm_name    = "master1.ramiel.fuku"
  node_name  = "ramiel"
  ipconfig0  = "ip=192.168.1.31/24,gw=192.168.1.1"
  memory     = 4096
  core_count = 2
}

module "master2" {
  source     = "../modules/proxmox-vm"
  vm_id      = 3002
  vm_name    = "master2.ireul.fuku"
  node_name  = "ireul"
  ipconfig0  = "ip=192.168.1.32/24,gw=192.168.1.1"
  memory     = 4096
  core_count = 2
}

module "master3" {
  source     = "../modules/proxmox-vm"
  vm_id      = 3003
  vm_name    = "master3.ireul.fuku"
  node_name  = "ireul"
  ipconfig0  = "ip=192.168.1.33/24,gw=192.168.1.1"
  disk_storage_name = "local-lvm"
  memory     = 4096
  core_count = 2
}
