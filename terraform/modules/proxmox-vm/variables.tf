variable "vm_name" {
  description = "Name of the VM"
  type        = string
}

variable "node_name" {
  description = "Name of the Proxmox node"
  type        = string
}

variable "vm_id" {
  description = "ID of the new VM"
  type        = number
}

variable "cloud_init_template" {
  description = "Cloud-init enabled template to be cloned"
  type        = string
  default     = "ci-debian12"
}

variable "ipconfig0" {
  description = "Default inet configuration"
  type        = string
}

variable "ci_username" {
  description = "Cloud-init username"
  type        = string
  default     = "ci"
}


variable "memory" {
  description = "Available RAM, in MB"
  type        = number
  default     = 512
}

variable "core_count" {
  description = "Available cores per socket"
  type        = number
  default     = 2

}

variable "network_bridge_name" {
  description = "Network bridge name"
  type        = string
  default     = "vmbr0"
}

variable "ssh_keys" {
  description = "SSH public keys to be provisioned"
  type        = string
  default     = <<EOF
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL++DKyzE0+zM6A4lpjWTNvntQnmEdA6FoOh6jIdsHMT catalin@jupiter
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGreLVacZyxq2EtgievpXgC/L7czKyJa/kWpgqDoPgnA phireh@GenPhireh
EOF
}

variable "disk_storage_name" {
  description = "Disk storage name"
  type        = string
  default     = "storage"
}