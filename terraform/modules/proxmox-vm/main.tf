terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.14"
    }
  }
}

resource "proxmox_vm_qemu" "vm" {
  name        = var.vm_name
  target_node = var.node_name
  vmid        = var.vm_id
  clone       = var.cloud_init_template
  os_type     = "cloudinit"
  qemu_os     = "other"
  ipconfig0   = var.ipconfig0
  cores       = var.core_count
  sockets     = 1
  pool        = "k3s"
  memory      = var.memory
  ciuser      = var.ci_username
  agent       = 0
  serial {
    id   = 0
    type = "socket"
  }
  disk {
    size    = "50G"
    storage = var.disk_storage_name
    type    = "scsi"
  }
  network {
    bridge = var.network_bridge_name
    firewall = false
    link_down = false
    model = "virtio"
    mtu = 0
    queues = 0
    rate = 0
    tag = -1
  }
  sshkeys = var.ssh_keys
  nameserver = "192.168.1.7 192.168.1.3"
}
